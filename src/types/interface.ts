export interface NavigationItem {
  name: string
  icon: string
  route: string
}

export interface PanelContentItem {
  title: string
  subTitle?: string
  time?: string
}
