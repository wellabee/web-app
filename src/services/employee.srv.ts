//import { Employee } from './../types/interface'
import { Employee } from '@/types/graphql/index'

type Query = {
  getEmployee(id: string): Employee
  getEmployees: [Employee]
}

import {
  ApolloClient,
  InMemoryCache,
  ApolloQueryResult,
  DocumentNode,
  gql
} from '@apollo/client/core'

const apolloClient = new ApolloClient({
  uri: 'http://localhost:4000',
  cache: new InMemoryCache()
})

const teamEmployees = gql`
  query Employees {
    getEmployees {
      id
      name
    }
  }
`

const employeeByIdQuery = (id: string) => gql`
  query EmployeeByIdQuery {
    getEmployee(id: "${id}") {
      id
      name
      profilePicture
      joinedDate
      location {
        name
      }
      courses {
        title
      }
      role {
        title
      }
      currentMood {
        title
        score
        date
      }
      drives {
        name
        score
        color
      }
      courses {
        title
      }
      projects {
        title
      }
      meetings {
        title
        time
        durationInMinutes
      }
    }
  }
`

const getGraphqlResults = (
  query: DocumentNode
): Promise<ApolloQueryResult<any>> => {
  return apolloClient.query({
    fetchPolicy: 'network-only',
    query: query
  })
}

const employees: Array<Employee> = [
  {
    id: '1',
    name: 'Monica Veen',
    profilePicture: 'HappyMonicaPicture.png',
    location: { id: '11', name: 'Utrecht' },
    joinedDate: new Date(),
    role: {
      id: '1',
      title: 'Front-end Developer'
    },
    currentMood: {
      title: 'Feeling happy',
      score: 7.2,
      date: new Date()
    },
    drives: [
      {
        name: 'AUTONOMY',
        score: 6.5,
        color: '#18C0FF'
      },
      {
        name: 'MASTERY',
        score: 7.1,
        color: '#FDB400'
      },
      {
        name: 'PURPOSE',
        score: 9.0,
        color: '#F72B60'
      }
    ],
    courses: [
      {
        id: '1',
        title: 'CSS - Basics to Advanced for Front-end development'
      }
    ],
    projects: [
      {
        id: '1',
        title: 'project 1'
      },
      {
        id: '2',
        title: 'project 2'
      }
    ],
    meetings: [
      {
        title: 'Daily Standup',
        time: new Date(),
        durationInMinutes: 30
      },
      {
        title: 'Project Branstorm',
        time: new Date(),
        durationInMinutes: 120
      }
    ]
  }
  // {
  //   id: '2',
  //   firstName: 'Jack',
  //   lastName: 'Sparrow',
  //   location: { id: '12', name: 'Caribbean Sea' }
  // }
]

export type TeamEmployeesResponse = {
  readonly getEmployees: Employee[]
}

export const getEmployees = async () => {
  return apolloClient.query<TeamEmployeesResponse>({
    query: teamEmployees
  })
}

export type EmployeeByIdResponse = {
  readonly getEmployee: Employee
}

export const getEmployee = async (id: string) => {
  return apolloClient.query<EmployeeByIdResponse>({
    query: employeeByIdQuery(id)
  })
}
