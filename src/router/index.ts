import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: import('../views/MyTeam.vue')
  },
  {
    path: '/my_team',
    name: 'My Team',
    component: () => import('../views/MyTeam.vue')
  },
  {
    path: '/employee_profile/:id',
    name: 'Profile',
    component: () => import('../views/EmployeeProfile.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
