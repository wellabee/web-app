const numberHumanized = (num: number): string =>
  num > 10 ? String(num) : `0${num}`

export const timeHumanized = (date: Date): string => {
  return `${numberHumanized(date.getHours())}:${numberHumanized(
    date.getMinutes()
  )}`
}
