export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: any;
};

export type Course = {
  __typename?: 'Course';
  id: Scalars['String'];
  title: Scalars['String'];
};


export type Drive = {
  __typename?: 'Drive';
  name: Scalars['String'];
  score: Scalars['Float'];
  color: Scalars['String'];
};

export type Employee = {
  __typename?: 'Employee';
  id: Scalars['String'];
  name: Scalars['String'];
  profilePicture: Scalars['String'];
  location: Location;
  joinedDate: Scalars['DateTime'];
  role: Role;
  currentMood: Mood;
  courses: Array<Course>;
  projects: Array<Project>;
  meetings: Array<Meeting>;
  drives: Array<Drive>;
};

export type Location = {
  __typename?: 'Location';
  id: Scalars['String'];
  name: Scalars['String'];
};

export type Meeting = {
  __typename?: 'Meeting';
  title: Scalars['String'];
  durationInMinutes: Scalars['Float'];
  time: Scalars['DateTime'];
};

export type Mood = {
  __typename?: 'Mood';
  title: Scalars['String'];
  score: Scalars['Float'];
  date: Scalars['DateTime'];
};

export type Project = {
  __typename?: 'Project';
  id: Scalars['String'];
  title: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  getEmployees?: Maybe<Array<Employee>>;
  getEmployee?: Maybe<Employee>;
};


export type QueryGetEmployeeArgs = {
  id: Scalars['String'];
};

export type Role = {
  __typename?: 'Role';
  id: Scalars['String'];
  title: Scalars['String'];
};
